package com.aliasadi.androidmvp.ui.details;

import com.aliasadi.androidmvp.data.movie.Movie;
import com.aliasadi.androidmvp.ui.base.BaseView;

public interface DetailsView extends BaseView {

    void showMovieDetails(Movie movie);

    void showDataUnavailableMessage();

}
