package com.aliasadi.androidmvp.data.movie.source.remote.services;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MovieService {

    private static final String URL = "https://demo6483760.mockable.io/";

    private MovieApi mMovieApi;

    private static MovieService singleton;

    private MovieService() {
        Retrofit mRetrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(URL).build();
        mMovieApi = mRetrofit.create(MovieApi.class);
    }

    public static MovieService getInstance() {
        if (singleton == null) {
            singleton = new MovieService();
        }
        return singleton;
    }

    public MovieApi getMovieApi() {
        return mMovieApi;
    }

}
